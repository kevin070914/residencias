package com.example.alfarosa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {

    ImageView imagen1, imagen2;
    //TextView bienvenido;
    //todo y fixme para comentar
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        imagen1 = (ImageView) findViewById(R.id.encabezado);
        imagen2 = (ImageView) findViewById(R.id.logo);
        //bienvenido = (TextView) findViewById(R.id.bienvenido);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER, WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);

        Animation animacion2 = AnimationUtils.loadAnimation(this, R.anim.dezplazamientoarriba);
        Animation animacion3 = AnimationUtils.loadAnimation(this, R.anim.parpadeo);

        //bienvenido.setAnimation((animacion2));
        imagen2.setAnimation(animacion3);

        //Pasar a la siguiente actividad
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intencion = new Intent(SplashScreen.this, IniciarSesion.class);
                startActivity(intencion);
                finish();
            }
        }, 4000);
    }
    /*
        if (siYaSeAbrioApp()){
            Intent intent = new Intent(SplashScreen.this, IniciarSesion.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {

            SharedPreferences.Editor editor = getSharedPreferences("slide", MODE_PRIVATE).edit();
            editor.putBoolean("slide", true);
            editor.commit();

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER, WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);

            Animation animacion2 = AnimationUtils.loadAnimation(this, R.anim.dezplazamientoarriba);
            Animation animacion3 = AnimationUtils.loadAnimation(this, R.anim.parpadeo);

            //bienvenido.setAnimation((animacion2));
            imagen2.setAnimation(animacion3);

            //Pasar a la siguiente actividad
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intencion = new Intent(SplashScreen.this, IniciarSesion.class);
                    startActivity(intencion);
                    finish();
                }
            }, 4000);
        }
    }
        private boolean siYaSeAbrioApp () {
            SharedPreferences sharedPreferences = getSharedPreferences("slide", MODE_PRIVATE);
            boolean resultado = sharedPreferences.getBoolean("slide", false);
            return resultado;
        }

     */
}