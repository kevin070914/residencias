package com.example.alfarosa.fragmentos;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.alfarosa.Home;
import com.example.alfarosa.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MasFragment extends Fragment {

    FrameLayout master2;

    CardView cartaMas, cartaMas2, cartaMas3;

    PerfilFragment SextoFragment = new PerfilFragment();
    VideosFragment SeptimoFragment = new VideosFragment();
    BannerFragment OctavoFragment = new BannerFragment();

    FragmentTransaction fragmentTransaction,fragmentTransaction2,fragmentTransaction3;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MasFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MasFragment newInstance(String param1, String param2) {
        MasFragment fragment = new MasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mas, container, false);
        cartaMas = (CardView) view.findViewById(R.id.cartaMas);
        cartaMas2 = (CardView) view.findViewById(R.id.cartaMas2);
        cartaMas3 = (CardView) view.findViewById(R.id.cartaMas3);
        master2 = view.findViewById(R.id.container2);
        accionPerfil();
        accionVideos();
        accionBanner();
        return view;
    }


    public void accionPerfil() {
        cartaMas3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container2, SextoFragment);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                /*
                cartaMas.setVisibility(View.GONE);
                cartaMas2.setVisibility(View.GONE);
                cartaMas3.setVisibility(View.GONE);
                fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container2, SextoFragment);
                fragmentTransaction.isAddToBackStackAllowed();
                fragmentTransaction.commit();

                 */
            }

        });
    }

    public void accionVideos() {
        cartaMas2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction2 = getFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.container2, SeptimoFragment);
                fragmentTransaction2.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();
            }

        });

    }
    public void accionBanner() {
        cartaMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction2 = getFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.container2, OctavoFragment);
                fragmentTransaction2.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();
            }

        });

    }
}
