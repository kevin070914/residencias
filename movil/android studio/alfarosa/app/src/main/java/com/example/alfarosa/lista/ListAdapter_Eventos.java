package com.example.alfarosa.lista;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alfarosa.R;

import java.util.List;

public class ListAdapter_Eventos extends RecyclerView.Adapter<ListAdapter_Eventos.ViewHolder> implements View.OnClickListener {

    public Context context;
    private View.OnClickListener listener;
    public List<ListElements_Eventos> mData;
    public LayoutInflater mInflater;


    public ListAdapter_Eventos(Context context, List<ListElements_Eventos> mData) {
        this.context = context;
        this.mData = mData;
        this.mInflater = LayoutInflater.from(context);
    }
    @Override
    public int getItemCount() {
        return this.mData.size();
    }
    
    
    @NonNull
    @Override
    public ListAdapter_Eventos.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.from(parent.getContext()).inflate(R.layout.card_eventos,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }



    public void setItem(List<ListElements_Eventos> items) {
        mData = items;
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter_Eventos.ViewHolder holder, int position) {
        holder.cv.setAnimation(AnimationUtils.loadAnimation(context,R.anim.slide));
        holder.bindData(this.mData.get(position));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, "Long click", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }



    @Override
    public void onClick(View view) {
        View.OnClickListener onClickListener = this.listener;
        if (onClickListener != null) {
            onClickListener.onClick(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView Txt_evento,Txt_Duracion,Txt_Fecha_creada;
        CardView cv;
        ImageView Img_card;

        
        public ViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            Img_card = itemView.findViewById(R.id.Img_card);
            Txt_evento = itemView.findViewById(R.id.Txt_evento);
            Txt_Duracion = itemView.findViewById(R.id.Txt_duracion);
            Txt_Fecha_creada = itemView.findViewById(R.id.Txt_Fecha_creada);
        }

        /* access modifiers changed from: package-private */
        public void bindData(ListElements_Eventos item) {
            Img_card.setBackgroundResource(item.getColor_contorno());
            Txt_evento.setText(item.getEvento());
            Txt_Duracion.setText(item.getDuracion());
            Txt_Fecha_creada.setText(item.getFecha_creada());
        }

    }
}
