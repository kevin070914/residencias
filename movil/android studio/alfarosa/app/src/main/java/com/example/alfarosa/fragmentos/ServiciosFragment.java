package com.example.alfarosa.fragmentos;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alfarosa.R;
import com.example.alfarosa.lista.ListAdapter_Eventos;
import com.example.alfarosa.lista.ListElements_Eventos;

import java.util.List;

public class ServiciosFragment extends Fragment {
    CardView cProgramacion;
    private List<ListElements_Eventos> elements;
    private ListAdapter_Eventos listAdapter;
    private RecyclerView recyclerView;


    public ServiciosFragment() {
        // Required empty public constructor
    }
    public static ServiciosFragment newInstance() {
        ServiciosFragment fragment = new ServiciosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_servicios, container, false);
        cProgramacion = (CardView) view.findViewById(R.id.cProgramacion);

        //Oyente de carta
        cProgramacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                View view1 = inflater.inflate(R.layout.dialogoserviciosempresa,container,false);
                alertDialogBuilder.setView(view1);
                alertDialogBuilder.create().show();
            }
        });
        return view;
    }
}