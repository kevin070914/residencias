package com.example.alfarosa.lista;

public class ListElements_Categoria {
    public String Categoria,Información;
    public int Color_contorno;

    public ListElements_Categoria(String categoria, String información, int color_contorno) {
        Categoria = categoria;
        Información = información;
        Color_contorno = color_contorno;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getInformación() {
        return Información;
    }

    public void setInformación(String información) {
        Información = información;
    }

    public int getColor_contorno() {
        return Color_contorno;
    }

    public void setColor_contorno(int color_contorno) {
        Color_contorno = color_contorno;
    }

    @Override
    public String toString() {
        return "ListElements_Categoria{" +
                "Categoria='" + Categoria + '\'' +
                ", Información='" + Información + '\'' +
                ", Color_contorno=" + Color_contorno +
                '}';
    }
}
