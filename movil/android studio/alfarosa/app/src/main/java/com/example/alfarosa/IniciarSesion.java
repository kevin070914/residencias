package com.example.alfarosa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class IniciarSesion extends AppCompatActivity {
    TextInputLayout textoCorreo, textoPassword;
    TextInputEditText cajaCorreo, cajaPassword;
    Button btningresar, btnCrearCuenta;

    String e, p;
    //Variable para que almacene la url o dominio
    //String HttpURI = "http://192.168.8.81/alfarosa/usuario.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        SharedPreferences preferences = getSharedPreferences("user",MODE_PRIVATE);
        //Instancia de método
        Principal(preferences.getString("correo",""),preferences.getString("clave",""));

        textoCorreo = (TextInputLayout) findViewById(R.id.textoCorreo);
        textoPassword = (TextInputLayout) findViewById(R.id.textoPassword);
        cajaCorreo = (TextInputEditText) findViewById(R.id.cajaCorreo);
        cajaPassword = (TextInputEditText) findViewById(R.id.cajaPassword);
        btningresar = (Button) findViewById(R.id.btningresar);
        btnCrearCuenta = (Button) findViewById(R.id.btnCrearCuenta);
    }
    public void Registrarte(View v){
        //Pasar datos a otra actividad
        Intent intencion = new Intent(this,Registro.class);
        startActivity(intencion);
    }
    public void Ingresa(View v){
        //Instancia de método
        Principal(cajaCorreo.getText().toString(),cajaPassword.getText().toString());
    }
    public void Principal(String correo, String clave){
        
        //Variable para que almacene la url o dominio
        String HttpURI = getString(R.string.directorio);
        //Obtener valores de ambos campos
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(IniciarSesion.this);
        e = correo;
        p = clave;
        //Validar campos "Para ver si están vacíos"
        if(e.isEmpty() || p.isEmpty()){
            Toast.makeText(getApplicationContext(),"Debes llenar ambos campos",
                    Toast.LENGTH_LONG).show();
            System.out.println(e);
            System.out.println(p);
        }
        else{
            //Mostrar el progressDialog
            progressDialog.setMessage("Procesando...");
            progressDialog.show();

            //Creación de la cadena a ejecutar en el webservice mediante volley
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpURI,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String serverResponse) {
                            //Ocultamos el progressDialog
                            progressDialog.dismiss();

                            try{
                                JSONObject obj = new JSONObject(serverResponse);
                                Boolean error = obj.getBoolean("error");
                                String mensaje = obj.getString("mensaje");
                                if(error==true){
                                    Toast.makeText(getApplicationContext(),mensaje,
                                            Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Acceso Correcto",
                                            Toast.LENGTH_LONG).show();
                                    System.out.println("VARIABLES..........................................................................................................................");
                                    System.out.println(e);
                                    System.out.println(p);
                                    Comprobar(e,p);
                                    //Acceder a la clase intent
                                    Intent intent1 = new Intent(getApplicationContext(),Home.class);
                                    //Mandar a ejecutar
                                    startActivity(intent1);
                                    finish();
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Ocultamos el progressDialog
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(),error.toString(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }){
                //Mapeo de los valores enviados en el webservice
                protected Map<String, String> getParams(){
                    Map<String,String> parametros = new HashMap<>();
                    parametros.put("correo", e);
                    parametros.put("clave", p);
                    parametros.put("opcion", "login");
                    return parametros;
                }
            };

            requestQueue.add(stringRequest);
        }
    }
    public  void recuperarClave(View view){
      primerALerta();
    }
    public void primerALerta(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogorecuperarclave, null);
        alertDialogBuilder.setView(v);
        alertDialogBuilder.create().show();
    }
    public void Comprobar(String correo, String clave){
        SharedPreferences sp1 = getSharedPreferences("user",Context.MODE_PRIVATE);
        SharedPreferences.Editor kevin;
        kevin = sp1.edit();
        kevin.putString("correo",correo);
        kevin.putString("clave",clave);
        kevin.commit();
    }
}
