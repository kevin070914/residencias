package com.example.alfarosa.fragmentos;

import android.animation.LayoutTransition;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.alfarosa.R;

public class NosotrosFragment extends Fragment {


    TextView txtSocio,txtSocio2,txtSocio3;
    LinearLayout layoutSocio,layoutSocio2, layoutSocio3;
    CardView cc8,cc9,cc10;
    ImageView flecha,flecha2,flecha3;


    public NosotrosFragment() {
        // Required empty public constructor
    }

    public static NosotrosFragment newInstance() {
        NosotrosFragment fragment = new NosotrosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nosotros, container, false);
        //TextView
        txtSocio = view.findViewById(R.id.txtSocio);
        txtSocio2 = view.findViewById(R.id.txtSocio2);
        txtSocio3 = view.findViewById(R.id.txtSocio3);


        //Layout
        layoutSocio = view.findViewById(R.id.layoutSocio);
        layoutSocio2 = view.findViewById(R.id.layoutSocio2);
        layoutSocio3 = view.findViewById(R.id.layoutSocio3);

        //CardView
        cc8 = view.findViewById(R.id.cc8);
        cc9 = view.findViewById(R.id.cc9);
        cc10 = view.findViewById(R.id.cc10);

        flecha = (ImageView) view.findViewById(R.id.flecha);
        flecha2 = (ImageView) view.findViewById(R.id.flecha2);
        flecha3 = (ImageView) view.findViewById(R.id.flecha3);


        //Asignamos animación a los layout correspondientes
        layoutSocio.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutSocio2.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutSocio3.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);


        //Oyentes de CardView
        cc8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtSocio.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutSocio,new AutoTransition());
                if (txtSocio.getVisibility() == View.VISIBLE){
                    flecha.setVisibility(View.VISIBLE);
                } else {
                    flecha.setVisibility(View.GONE);
                }
                txtSocio.setVisibility(v);
            }
        });
        cc9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtSocio2.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutSocio2,new AutoTransition());
                if (txtSocio2.getVisibility() == View.VISIBLE){
                    flecha2.setVisibility(View.VISIBLE);
                } else {
                    flecha2.setVisibility(View.GONE);
                }
                txtSocio2.setVisibility(v);
            }
        });

        cc10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtSocio3.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutSocio3,new AutoTransition());
                if (txtSocio3.getVisibility() == View.VISIBLE){
                    flecha3.setVisibility(View.VISIBLE);
                } else {
                    flecha3.setVisibility(View.GONE);
                }
                txtSocio3.setVisibility(v);
            }
        });
        return view;
    }
}