package com.example.alfarosa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Registro extends AppCompatActivity {
    TextInputLayout IndicaCorreo, IndicaNombre, IndicaApellidoP, IndicaApellidoM, IndicaPassword, IndicaPassword2;
    TextInputEditText IngresarCorreo, IngresarNombre, IngresarApellidoP, IngresarApellidoM, IngresarPassword, IngresarPassword2;
    Button btnRegistrar, btnVolver, btnVerTerminosCondiciones;
    CheckBox checarEstadoTerminosCondiciones;

    //Variable para que almacene la url o dominio
    //String HttpURI = "http://192.168.8.81/alfarosa/usuario.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        IndicaCorreo = (TextInputLayout) findViewById(R.id.IndicaCorreo);
        IndicaNombre = (TextInputLayout) findViewById(R.id.IndicaNombre);
        IndicaApellidoP = (TextInputLayout) findViewById(R.id.IndicaApellidoP);
        IndicaApellidoM = (TextInputLayout) findViewById(R.id.IndicaApellidoM);
        IndicaPassword = (TextInputLayout) findViewById(R.id.IndicaPassword);
        IndicaPassword2 = (TextInputLayout) findViewById(R.id.IndicaPassword2);
        IngresarCorreo = (TextInputEditText) findViewById(R.id.IngresarCorreo);
        IngresarNombre = (TextInputEditText) findViewById(R.id.IngresarNombre);
        IngresarApellidoP = (TextInputEditText) findViewById(R.id.IngresarApellidoP);
        IngresarApellidoM = (TextInputEditText) findViewById(R.id.IngresarApellidoM);
        IngresarPassword = (TextInputEditText) findViewById(R.id.IngresarPassword);
        IngresarPassword2 = (TextInputEditText) findViewById(R.id.IngresarPassword2);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnVolver = (Button) findViewById(R.id.btnVolver);
        btnVerTerminosCondiciones = (Button) findViewById(R.id.btnVerTerminosCondiciones);
        checarEstadoTerminosCondiciones = (CheckBox) findViewById(R.id.checarEstadoTerminosCondiciones);

        //Validaciones de campos
        IngresarNombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    esNombreValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        IngresarApellidoP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                esApellidoPValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        IngresarApellidoM.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                esApellidoMValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        IngresarCorreo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                esCorreoValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        IngresarPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                esClaveValida(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        IngresarPassword2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                esClaveValida2(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
        public void Volver (View v){
            Intent intencion = new Intent(getApplicationContext(), IniciarSesion.class);
            startActivity(intencion);
            finish();
        }

        public void Registrar(View v){
            //Obtener valores de ambos campos
            String nombre = IngresarNombre.getText().toString(); // nombre
            String apellidoP = IngresarApellidoP.getText().toString();//apellidop
            String apellidoM = IngresarApellidoM.getText().toString(); //apellidom
            String correo =   IngresarCorreo.getText().toString(); // correo
            String clave1 =  IngresarPassword.getText().toString(); //clave 1
            String clave2 = IngresarPassword2.getText().toString(); //clave 2

            Boolean aceptaTerminos = checarEstadoTerminosCondiciones.isChecked(); // Aceptar terminos y condiciones

            //variables para validaciones
            String tilnombre = IndicaNombre.getEditText().getText().toString();
            String tilapellidop = IndicaApellidoP.getEditText().getText().toString();
            String tilapellidoM = IndicaApellidoM.getEditText().getText().toString();
            String tilcorreo = IndicaCorreo.getEditText().getText().toString();
            String tilpassword = IndicaPassword.getEditText().getText().toString();
            String tilpassword2 = IndicaPassword2.getEditText().getText().toString();


            boolean a = esClaveValida(tilpassword);
            boolean b = esClaveValida2(tilpassword2);
            boolean c = esNombreValido(tilnombre);
            boolean d = esApellidoPValido(tilapellidop);
            boolean e = esApellidoMValido(tilapellidoM);
            boolean f = esCorreoValido(tilcorreo);

            //Variable para que almacene la url o dominio
            String HttpURI = getString(R.string.directorio);

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            ProgressDialog progressDialog;
            progressDialog = new ProgressDialog(Registro.this);


            //Validar campos "Para ver si están vacíos"
            if(nombre.isEmpty() || apellidoP.isEmpty() || apellidoM.isEmpty() || correo.isEmpty() || clave1.isEmpty() || clave2.isEmpty() || aceptaTerminos.equals(false)){
                Toast.makeText(getApplicationContext(),"Falto algo por llenar, verifica",
                        Toast.LENGTH_LONG).show();
            }
            else if (a && b && c && d && e && f){
                //Mostrar el progressDialog
                progressDialog.setMessage("Procesando...");
                progressDialog.show();

                //Creación de la cadena a ejecutar en el webservice mediante volley
                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpURI,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String serverResponse) {
                                //Ocultamos el progressDialog
                                progressDialog.dismiss();
                                try{
                                    JSONObject obj = new JSONObject(serverResponse);
                                    Boolean error2 = obj.getBoolean("error2");
                                    String mensaje2 = obj.getString("mensaje2");

                                    if(error2==true){
                                        Toast.makeText(getApplicationContext(),mensaje2,
                                                Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(getApplicationContext(),"¡Felicidades, cuenta creada!",
                                                Toast.LENGTH_LONG).show();
                                        limpiarCampos();
                                        Intent intent5 = new Intent(getApplicationContext(), IniciarSesion.class);
                                        startActivity(intent5);
                                        finish();
                                    }
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Ocultamos el progressDialog
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),""+error,
                                        Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                    //Mapeo de los valores enviados en el webservice
                    protected Map<String, String> getParams(){
                        Map<String,String> parametros2 = new HashMap<>();
                        //Variables de la base de datos y de android studio
                        parametros2.put("nombre", nombre);
                        parametros2.put("apellido_p",apellidoP );
                        parametros2.put("apellido_m", apellidoM);
                        parametros2.put("correo", correo);
                        parametros2.put("clave", clave1);
                        parametros2.put("clave2", clave2);
                        parametros2.put("opcion", "registro");
                        return parametros2;
                    }
                };

                requestQueue.add(stringRequest);
            }
        }

    public void leerTerminos (View view){
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();
        // Inflar y establecer el layout para el dialogo
        // Pasar nulo como vista principal porque va en el diseño del diálogo
        View v = inflater.inflate(R.layout.terminoscondicionesaviso, null);
        //builder.setView(inflater.inflate(R.layout.dialog_signin, null))
        Button cerrarDialogo = (Button)v.findViewById(R.id.cerrarDialogo);
        builder.setView(v);
        alertDialog = builder.create();
        alertDialog.show();
        // Add action buttons
        cerrarDialogo.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Cerrar dialogo
                        alertDialog.dismiss();
                    }
                }
        );
    }

    public void limpiarCampos(){
            IngresarNombre.setText("");
            IngresarApellidoP.setText("");
            IngresarApellidoM.setText("");
            IngresarCorreo.setText("");
            IngresarPassword.setText("");
            IngresarPassword2.setText("");
        }
    public boolean esNombreValido(String n) {
        Pattern patron = Pattern.compile("^[a-zA-Z]+$");
        if (!patron.matcher(n).matches()) {
            IndicaNombre.setError("Ingresa nombre válido");
            return false;
        } else {
            IndicaNombre.setError(null);
        }
        return true;
    }
    public boolean esApellidoPValido(String ap) {
        Pattern patron2 = Pattern.compile("^[a-zA-Z]+$");
        if (!patron2.matcher(ap).matches()) {
            IndicaApellidoP.setError("Ingresa un apellido paterno válido");
            return false;
        } else {
            IndicaApellidoP.setError(null);
        }
        return true;
    }
    public boolean esApellidoMValido(String am) {
        Pattern patron3 = Pattern.compile("^[a-zA-Z]+$");
        if (!patron3.matcher(am).matches()) {
            IndicaApellidoM.setError("Ingresa un apellido materno válido");
            return false;
        } else {
            IndicaApellidoM.setError(null);
        }
        return true;
    }
    public boolean esCorreoValido(String c) {
        Pattern patron4 = Pattern.compile("^[_a-z0-zA-Z0-9-]+(.[_a-z0-zA-Z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
        if (!patron4.matcher(c).matches()) {
            IndicaCorreo.setError("Correo inválido");
            return false;
        } else {
            IndicaCorreo.setError(null);
        }
        return true;
    }
    public boolean esClaveValida(String clave) {
        Pattern patron5 = Pattern.compile("^[a-zA-Z0-9]+$");
        if (!patron5.matcher(clave).matches() || clave.length() < 5) {
            IndicaPassword.setError("No puedes usar está contraseña");
            return false;
        } else {
            IndicaPassword.setError(null);
        }
        return true;
    }
    public boolean esClaveValida2(String clave2) {
        Pattern patron6 = Pattern.compile("^[a-zA-Z0-9]+$");
        if (!patron6.matcher(clave2).matches() || clave2.length() < 5) {
            IndicaPassword2.setError("No puedes usar está contraseña");
            return false;
        } else {
            IndicaPassword2.setError(null);
        }
        return true;
    }
    }