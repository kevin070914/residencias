package com.example.alfarosa.lista;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class ListElements_Eventos {
    public String Evento,Duracion,Fecha_creada;
    public int Color_contorno;

    public ListElements_Eventos(String evento, String duracion, String fecha_creada, int color_contorno) {
        Evento = evento;
        Duracion = duracion;
        Fecha_creada = fecha_creada;
        Color_contorno = color_contorno;
    }

    public String getEvento() {
        return Evento;
    }

    public void setEvento(String evento) {
        Evento = evento;
    }

    public String getDuracion() {
        return Duracion;
    }

    public void setDuracion(String duracion) {
        Duracion = duracion;
    }

    public String getFecha_creada() {
        return Fecha_creada;
    }

    public void setFecha_creada(String fecha_creada) {
        Fecha_creada = fecha_creada;
    }

    public int getColor_contorno() {
        return Color_contorno;
    }

    public void setColor_contorno(int color_contorno) {
        Color_contorno = color_contorno;
    }

    @Override
    public String toString() {
        return "ListElements_Eventos{" +
                "Evento='" + Evento + '\'' +
                ", Duracion='" + Duracion + '\'' +
                ", Fecha_creada='" + Fecha_creada + '\'' +
                ", Color_contorno=" + Color_contorno +
                '}';
    }
}
