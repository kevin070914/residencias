package com.example.alfarosa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.alfarosa.fragmentos.InicioFragment;
import com.example.alfarosa.fragmentos.MasFragment;
import com.example.alfarosa.fragmentos.ServiciosFragment;
import com.example.alfarosa.fragmentos.NosotrosFragment;
import com.example.alfarosa.fragmentos.SoporteFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Home extends AppCompatActivity {
    BottomNavigationView mBottomNavigation;
    Button btnCerrarSesion;

    InicioFragment PrimerFragmento = new InicioFragment();
    ServiciosFragment SegundoFragmento = new ServiciosFragment();
    NosotrosFragment TercerFragmento = new NosotrosFragment();
    SoporteFragment CuartoFragment = new SoporteFragment();
    MasFragment QuintoFragment = new MasFragment();

    FragmentTransaction transaction ;
    FrameLayout master;

    ImageView Systemkw;
    

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
        master = findViewById(R.id.container);

        Systemkw = findViewById(R.id.Txt_sytem_info);

        mBottomNavigation = findViewById(R.id.menu_navigation);
        mBottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){
                    case R.id.menu_inicio:
                        loadFragment(PrimerFragmento);
                        return true;
                    case R.id.menu_servicios:
                        loadFragment(SegundoFragmento);
                        return true;
                    case R.id.menu_conocenos:
                        loadFragment(TercerFragmento);
                        return true;
                    case R.id.menu_soporte:
                        loadFragment(CuartoFragment);
                        return true;
                    case R.id.menu_mas:
                        loadFragment(QuintoFragment);
                        return true;

                }
                return false;
            }
        });
        mBottomNavigation.setSelectedItemId(R.id.menu_inicio);
        //setear aquí para que el listener muestre el fragment inicial al cargarse la pantalla
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Cierra la activity en que se encuentra el usuario y regresa a la activity principal
                salir();
            }
        });
        Systemkw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
                LayoutInflater inflater = getLayoutInflater();
                View card_view = inflater.inflate(R.layout.card_info_extra, null);
                builder.setView(card_view);
                AlertDialog dialog = builder.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
            }
        });
    }
    //Cargar fragmentos
    public void loadFragment(Fragment fragment){
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    private void salir (){
        SharedPreferences sp1 = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor kevin;
        kevin = sp1.edit();
        kevin.putString("correo",null);
        kevin.putString("clave",null);
        kevin.commit();
        Intent intencion = new Intent(getApplicationContext(), IniciarSesion.class);
        startActivity(intencion);
        finish();
    }


}