package com.example.alfarosa.lista;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alfarosa.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.transition.Hold;

import java.util.List;

public class ListAdapter_Categorias extends RecyclerView.Adapter<ListAdapter_Categorias.ViewHolder> implements View.OnClickListener {

    public Context context;
    public List<ListElements_Categoria> mData;
    public LayoutInflater mInflater;

    public ListAdapter_Categorias(Context context, List<ListElements_Categoria> mData) {
        this.context = context;
        this.mData = mData;
        this.mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ListAdapter_Categorias.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.from(parent.getContext()).inflate(R.layout.card_categorias,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.cv.setAnimation(android.view.animation.AnimationUtils.loadAnimation(context,R.anim.slide));
    holder.bindData(this.mData.get(position));

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show();
        }
    });
    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            Toast.makeText(context, "Long click", Toast.LENGTH_SHORT).show();
            return true;
        }
    });
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        super.onViewRecycled(holder);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onClick(View view) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView Txt_categoria,Txt_descripcion;
        CardView cv;
        ImageView Img_card;


        public ViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            Img_card = itemView.findViewById(R.id.Img_card);
            Txt_categoria = itemView.findViewById(R.id.Txt_categoria);
            Txt_descripcion = itemView.findViewById(R.id.Txt_descripcion);
        }

        public void bindData(ListElements_Categoria item) {
            Img_card.setBackgroundResource(item.getColor_contorno());
            Txt_descripcion.setText(item.getInformación());
            Txt_categoria.setText(item.getCategoria());
            Txt_descripcion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mas = "@android:drawable/arrow_down_float";
                    String menos = "@android:drawable/arrow_up_float";
                    int img_resource_mas = context.getResources().getIdentifier(mas,null, context.getPackageName());
                    int img_resource_menos = context.getResources().getIdentifier(menos,null, context.getPackageName());


                    Log.e("Uri obtenido","ESTO: "+Txt_descripcion.getMaxLines());
                    if (Txt_descripcion.getMaxLines() == 1){
                        Txt_descripcion.setCompoundDrawablesWithIntrinsicBounds(img_resource_mas, 0, 0, 0);
                        Txt_descripcion.setMaxLines(150);
                    }else {
                        Txt_descripcion.setCompoundDrawablesWithIntrinsicBounds(img_resource_menos, 0, 0, 0);
                        Txt_descripcion.setMaxLines(1);
                    }




                }
            });
        }

    }
}
