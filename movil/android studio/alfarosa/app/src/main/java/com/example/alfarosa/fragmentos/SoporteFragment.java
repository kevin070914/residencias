package com.example.alfarosa.fragmentos;

import android.animation.LayoutTransition;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alfarosa.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SoporteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SoporteFragment extends Fragment {

    TextView txtSoporte,txtMantenimiento,txtInfoAdicional,txtUbicacion,txtHorarioAtencion,txtContacto;
    LinearLayout layoutSoporte,layoutMantenimiento,layoutInfoAdicional,layoutUbicacion,layoutHorarioAtencion,layoutContacto;
    CardView cs1,cs2,cs3,cs4,cs5,cs6;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SoporteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SoporteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SoporteFragment newInstance(String param1, String param2) {
        SoporteFragment fragment = new SoporteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_soporte, container, false);

        //TextView
        txtSoporte = view.findViewById(R.id.txtSoporte);
        txtMantenimiento = view.findViewById(R.id.txtMantenimiento);
        txtInfoAdicional = view.findViewById(R.id.txtInfoAdicional);
        txtUbicacion = view.findViewById(R.id.txtUbicacion);
        txtHorarioAtencion = view.findViewById(R.id.txtHorarioAtencion);
        txtContacto = view.findViewById(R.id.txtContacto);


        //Layout
        layoutSoporte = view.findViewById(R.id.layoutSoporte);
        layoutMantenimiento = view.findViewById(R.id.layoutMantenimiento);
        layoutInfoAdicional = view.findViewById(R.id.layoutInfoAdicional);
        layoutUbicacion = view.findViewById(R.id.layoutUbicacion);
        layoutHorarioAtencion = view.findViewById(R.id.layoutHorarioAtencion);
        layoutContacto = view.findViewById(R.id.layoutContacto);

        //CardView
        cs1 = view.findViewById(R.id.cs1);
        cs2 = view.findViewById(R.id.cs2);
        cs3 = view.findViewById(R.id.cs3);
        cs4 = view.findViewById(R.id.cs4);
        cs5 = view.findViewById(R.id.cs5);
        cs6 = view.findViewById(R.id.cs6);

        //Asignamos animación a los layout correspondientes
        layoutSoporte.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutMantenimiento.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutInfoAdicional.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutUbicacion.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutHorarioAtencion.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
        layoutContacto.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);


        //Oyentes de CardView
        cs1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtSoporte.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutSoporte,new AutoTransition());
                txtSoporte.setVisibility(v);
            }
        });

        cs2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtMantenimiento.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutMantenimiento,new AutoTransition());
                txtMantenimiento.setVisibility(v);
            }
        });

        cs3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtInfoAdicional.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutInfoAdicional,new AutoTransition());
                txtInfoAdicional.setVisibility(v);
            }
        });

        cs4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtUbicacion.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutUbicacion,new AutoTransition());
                txtUbicacion.setVisibility(v);
            }
        });

        cs5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtHorarioAtencion.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutHorarioAtencion,new AutoTransition());
                txtHorarioAtencion.setVisibility(v);
            }
        });

        cs6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int v = (txtContacto.getVisibility() == View.GONE)? View.VISIBLE: View.GONE;
                TransitionManager.beginDelayedTransition(layoutContacto,new AutoTransition());
                txtContacto.setVisibility(v);
            }
        });
        return view;
    }
}